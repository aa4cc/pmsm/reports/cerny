% LCThesis.cls
% Inspired by templates by Loi Do and Saurabh Garg
% Also inpired by:
%   https://www.overleaf.com/learn/latex/Writing_your_own_class
% 	https://tex.stackexchange.com/questions/8351/what-do-makeatletter-and-makeatother-do
% 	https://tex.stackexchange.com/questions/268406/how-to-color-section-number-and-section-name-with-different-colors/268407#268407




% -----------------------------------------------------------------------------
% Define LCThesis class
% -----------------------------------------------------------------------------

% Set LaTeX to class definition mode
\NeedsTeXFormat{LaTeX2e}

% Define the class
\ProvidesClass{LCThesis}[2020/02/09 LCThesis class]

% Import xkeyval package which defines macros for setting class keys and options
\RequirePackage{xkeyval}

% Use package ifthen to use if command in this class
\RequirePackage{ifthen}




% -----------------------------------------------------------------------------
% Define the CTU in Prague house style colors
% -----------------------------------------------------------------------------

% Use package for defining colors
\RequirePackage[usenames,dvipsnames,hyperref]{xcolor}

% Define CTU colors
\definecolor{ctu-white}{cmyk}{0,0,0,0}					% White
\definecolor{ctu-dark-blue}{cmyk}{1,0.43,0,0}			% Dark blue (Pantone 300)
\definecolor{ctu-black}{cmyk}{0,0,0,1}					% Black (Process Black 100 perc)
\definecolor{ctu-light-blue}{cmyk}{0.59,0.17,0,0}		% Light blue (Pantone 284)
\definecolor{ctu-grey}{cmyk}{0,0,0,0.5}					% Grey (Process Black 50 perc)
\definecolor{ctu-pantone383}{cmyk}{0.35,0,1,0.2}		% Light green (Pantone 383)
\definecolor{ctu-red}{cmyk}{0.02,1,0.82,0.06}			% Red (Pantone 186)
\definecolor{ctu-orange}{cmyk}{0,0.78,1,0}				% Orange (Pantone 166)
\definecolor{ctu-light-cyan}{cmyk}{0.84,0,0.38,0}		% Light cyan (Pantone 326)
\definecolor{ctu-dark-cyan}{cmyk}{0.86,0.2,0.32,0.53}	% Dark cyan (Pantone 5473)
\definecolor{ctu-magenta}{cmyk}{0.13,1,0.5,0.3}		    % Magenta (Pantone 7420)
\definecolor{ctu-pink}{cmyk}{0,0.78,0.08,0}				% Pink (Pantone 212)
\definecolor{ctu-yellow}{cmyk}{0,0.35,1,0}				% Yellow (Pantone 130)

% Define other colors
\definecolor{lc-dark-blue0}{cmyk}{0.9,0.5,0,0.3}		% Dark blue
\definecolor{lc-dark-blue1}{cmyk}{0.95,0.7,0,0.37}		% Dark blue
\definecolor{lc-dark-blue2}{cmyk}{0.95,0.7,0,0.65}		% Dark blue



% -----------------------------------------------------------------------------
% Define class options (default values are in [] brackets)
% -----------------------------------------------------------------------------

% ------------------------------
% Define option for formatting for [screen] and print.
% When formatting for screen colors are used for links, tables, and algorithms.
% When formatting for print black color is used for links, tables, and algorithms.
\define@choicekey*[LC]{thesis}{media}{screen,print}[screen]{\def \LCIntMedia{#1}}
\setkeys[LC]{thesis}{media}

% ------------------------------
% Define line spacing. The valid values are [onehalf] and double.
% Note that before using \LCLineSpacing setspace package must be loaded.
\define@choicekey*[LC]{thesis}{linespacing}[\val\nr]{onehalf,double}[onehalf]{
	\ifcase\nr\relax
		\def \LCIntLineSpacing{\onehalfspacing}
	\or
		\def \LCIntLineSpacing{\doublespacing}
	\fi
}
\setkeys[LC]{thesis}{linespacing}

% ------------------------------
% Define font size. The valid values are [11pt] and 12pt.
\define@choicekey*[LC]{thesis}{fontsize}{11pt,12pt}[11pt]{\def \LCIntFontSize{#1}}
\setkeys[LC]{thesis}{fontsize}

% ------------------------------
% Define font family to be used. Default is cm.
\define@choicekey*[LC]{thesis}{font}{cm,times,helvet,palatino}[cm]{\def \LCIntFont{#1}}
\setkeys[LC]{thesis}{font}

% ------------------------------
% Define open. The valid values are [right] and any.
% Right makes chapters and entries in frontmatter begin  only on right hand pages 
% or on the next page available. Any puts them on the next available page.
\define@choicekey*[LC]{thesis}{open}[\val\nr]{right,any}[right]{
	\ifcase\nr\relax
		\def \LCIntOpen {openright}
	\or
		\def \LCIntOpen {openany}
	\fi
}
\setkeys[LC]{thesis}{open}

% ------------------------------
% Define bibliography style
% Specifies whether basic bibliography or fancy bibliography style is to be used.
\define@choicekey*[LC]{thesis}{bibstyle}{basic,fancy}[basic]{\def \LCIntBibStyle{#1}}
\setkeys[LC]{thesis}{bibstyle}

% ------------------------------
% Define pageside. The valid values are oneside and [twoside].
% Specifies whether double or single sided output should be generated.
\define@choicekey*[LC]{thesis}{pageside}{oneside,twoside}[twoside]{\def \LCIntPageSide{#1}}
\setkeys[LC]{thesis}{pageside}





% ------------------------------
% Process all options defined above with default values.
\ProcessOptionsX[LC]<thesis>




% -----------------------------------------------------------------------------
% Load report class
% -----------------------------------------------------------------------------
\LoadClass[a4paper, \LCIntFontSize, \LCIntOpen, \LCIntPageSide]{report}




% -----------------------------------------------------------------------------
% Fonts
% -----------------------------------------------------------------------------

% Set input character encoding to UTF8
\RequirePackage[utf8]{inputenc}

% Use Babel package to allow for special characters and for
% proper english hyphenation  
\RequirePackage[english]{babel}

% Use csquotes to ensure quoted texts are typeset according to the rules of
% used language
\RequirePackage{csquotes}

% Use Latin Modern font which is enhanced version of the
% default Computer Modern font
\RequirePackage{lmodern}

% Set font encoding to T1 which allows for accented characters
% to be represented as actual characters (this enables proper
% hyphenation of words containing such characters and also
% such words can be properly copy-and-pasted from the output pdf)
\RequirePackage[T1]{fontenc}

% Improve typesetting and text appearance
\RequirePackage{microtype}    

% Support for arbitrary font size for cm
\RequirePackage{fix-cm}   

% Package containing some special symbols, e.g. copyright
\RequirePackage{textcomp}    




% -----------------------------------------------------------------------------
% Text format
% -----------------------------------------------------------------------------

% Starting a chapter with a big letter
%\RequirePackage{lettrine}

% Load package for formatting table of contents before parskip.
\RequirePackage[titles, subfigure]{tocloft}

% Use parskip package to replace paragraph indentation
% by vertical space between paragraphs.
%\RequirePackage{parskip}

% Set length of the vertical space between paragraphs
%\setlength{\parskip}{5mm}

% Set line spacing according to given option linespacing
\RequirePackage{setspace}
\LCIntLineSpacing

% Nicer captions for figures and tables
% Caption package must be included before subfig and hyperref.
% Change the font figure and table label to bold
\RequirePackage[labelfont=bf, format=hang]{caption}




% -----------------------------------------------------------------------------
% Page layout
% -----------------------------------------------------------------------------

% Use package geometry to define page layout (margins)
\RequirePackage[a4paper]{geometry}
\ifthenelse{\equal{\LCIntPageSide}{twoside}} {
	\geometry{verbose, tmargin=2.5cm, bmargin=2.5cm, lmargin=3cm, rmargin=2.5cm, includehead, marginparwidth=1.5cm, \LCIntPageSide}
}{
	\geometry{verbose, tmargin=2.5cm, bmargin=2.5cm, lmargin=3cm, rmargin=2.5cm, includehead, marginparwidth=1.5cm}
}

	
% Define LCIntClearDoublePage to clear double page or a single page
% according to open parameter given by user (open equals openright or openany)
\ifthenelse{\equal{\LCIntOpen}{openright}}
{
	% For adding extra blank page, if necessary, after chapter.
	\RequirePackage{emptypage}
	
	% Define a command to leave a blank page. This is used in frontmatter to add 
	% an empty page between ack, abstract, toc, lof and lot.
	\newcommand{\LCIntClearDoublePage}{\clearpage{\pagestyle{empty}\cleardoublepage}}
}
{
	\newcommand{\LCIntClearDoublePage}{\clearpage}
}

% Fix footnote spacing
\setlength{\footnotesep}{0.5cm}   % Distance between two footnotes.
\setlength{\skip\footins}{0.75cm}  % Distance between last line of text and first footnote.

% By default Latex centers images vertically on a float page.
% Modify Latex internal variables so that figures are placed from top.
%\makeatletter
    \setlength{\@fptop}{0.25cm}
    \setlength{\@fpsep}{0.25cm}
%\makeatother

% Set distances between floating objects (figures, tables, ...) and text
\setlength{\textfloatsep}{0.5cm} % Set the distance between a float and text.
\setlength{\floatsep}{0.5cm}     % Set the distance between two floats.

% Prevent latex from adding extra space between paragraphs so that
% the last line is at the bottom margin on each page.
\raggedbottom

% Define horizontal spacing between subfigures.
\newcommand{\LCIntHSpaceBetweenSubfloats}{\hspace{0.5cm}}

% Package that improves the interface for defining floating objects such as 
% figures and tables.
% Must be done before hyperref.
\RequirePackage{float} 




% -----------------------------------------------------------------------------
% Table format
% -----------------------------------------------------------------------------

% Set table layout and design.
\RequirePackage{booktabs, colortbl}   % Tables.
\RequirePackage{tabularx}             % Auto column sizing.

\renewcommand{\arraystretch}{1}       % Set space between rows in a table.
\renewcommand{\tabcolsep}{0.20cm}     % Set space between columns in a table.
\heavyrulewidth = 0.15em              % Set width of heavy rules.
\lightrulewidth = 0.07em              % Set width of light rules.
\abovetopsep    = 0.1cm               % Set separation between caption and top rule.
\aboverulesep   = 0.4ex               % Set separation to use above a rule.
\belowrulesep   = 0.4ex               % Set separation to use below a rule.

% Set color for table rules (if you want)
%\ifthenelse{\equal{\LCIntMedia}{screen}}{\arrayrulecolor{ctu-dark-blue}}{}
%\ifthenelse{\equal{\LCIntMedia}{print}}{\arrayrulecolor{ctu-dark-blue}}{}
\newcolumntype{C}{>{\centering\arraybackslash}X} % centered version of "X" type
\setlength{\extrarowheight}{1pt}




% -----------------------------------------------------------------------------
% Algorithm format
% -----------------------------------------------------------------------------
\RequirePackage[algo2e, ruled, linesnumbered, algochapter]{algorithm2e}
\DontPrintSemicolon                      % Dont print semicolons at end of lines.
\algoheightrule       = \heavyrulewidth  % Set the width of the top and bottom rules.
\algotitleheightrule  = \lightrulewidth  % Set the width of the middle rule.
\SetAlgoInsideSkip{medskip}              % Set distance between middle rule and algorithm.
\interspacetitleruled = 0.2cm            % Set distance between caption and rules.
\setlength{\algomargin}{2.25em}          % Set the margin of the algorithm text.
\SetNlSkip{1.25em}                       % Set the spacing between line numbers and text.

\newenvironment{LCAlgorithm}[1][t]
{%
	\begin{algorithm2e}[#1]
    \linespread{1.3} % Set the line spacing to one and half.
    \selectfont      % The linespread is effective only after selectfont.%
}
{%
	\end{algorithm2e}
}




% -----------------------------------------------------------------------------
% Fancy header format
% -----------------------------------------------------------------------------

% Import fancyhdr package
\RequirePackage{fancyhdr}

% Set page style to fancy.
\pagestyle{fancy}

% By default fancyhdr converts the chapter headings to uppercase,
% so restore the chapter casing.
\renewcommand{\chaptermark}[1]{\markboth{\@chapapp\ \thechapter\quad #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\quad #1}{}}
% Reset header style
\fancyhead{} 
% Top left on left (even) page - page number and chapter name
\fancyhead[LE]{{\large\bfseries\thepage}\qquad\leftmark}
% Top right on right (odd) page - section name and page number
\fancyhead[RO]{\nouppercase{\rightmark}\qquad\large\bfseries\thepage}
% Line below header
\renewcommand{\headrulewidth}{1pt}
% Increase the header height
\addtolength{\headheight}{10pt}

% Disable footer.
\fancyfoot{}
\renewcommand{\footrulewidth}{0pt}

% Define header and footer for plain pages.
\fancypagestyle{plain}
{
    % Disable header.
    \fancyhead{}
    \renewcommand{\headrulewidth}{0pt}
    
    % Footer contains the page number on right side.
    \fancyfoot{}
    \fancyfoot[R]{\bfseries\thepage}
    \renewcommand{\footrulewidth}{0pt}
}
  



% -----------------------------------------------------------------------------
% Chapter heading
% -----------------------------------------------------------------------------

% Change the appearance of chapter headers and section titles.
% Set the format of the part, section and subsection titles.
\RequirePackage[raggedright]{titlesec}

% Define chapter heading the same way as dafault settings
% (in order to be able to change spacing)
\titleformat{\chapter}[display]
{\normalfont\LARGE\bfseries\raggedright}{\chaptertitlename\ \thechapter}{20pt}{\huge}

% Change spacing of chapter heading
\titlespacing*{\chapter}{0pt}{-20pt}{40pt}




% -----------------------------------------------------------------------------
% Set hyperlink settings
% -----------------------------------------------------------------------------

% Use package for embedding URL's in document.
\RequirePackage[hyphens]{url} 

% Enable hyperlinks within the PDF.
\RequirePackage[unicode=true, bookmarks=true, bookmarksnumbered=true,
bookmarksopen=false, pdfstartview=Fit, pdfborder={0 0 1}, colorlinks=true, linktoc=page, pdfpagemode=UseNone]{hyperref}

% Define the colors of the hyperlink.
\ifthenelse{\equal{\LCIntMedia}{screen}}
{
	\hypersetup
	{
		colorlinks = true,
		linkcolor  = {lc-dark-blue1},
		citecolor  = {lc-dark-blue1},
		urlcolor   = {lc-dark-blue1}
	}
}{}
\ifthenelse{\equal{\LCIntMedia}{print}}
{
	\hypersetup{colorlinks=false,pdfborder={0 0 0}}
}{}

% Define proerties describing this PDF (PDF metadata).
\newcommand{\LCIntSetupPdfProps}
{
	\hypersetup
	{
		pdfauthor   = \LCIntAuthor, 
		pdftitle    = \LCIntTitle, 
		pdfsubject  = \LCIntSubject, 
		pdfkeywords = \LCIntKeywordsEn
	}
}




% -----------------------------------------------------------------------------
% Define class parameters to be set by user
% -----------------------------------------------------------------------------

% Use package for loading that has command \CatchFileDef.
% This command works as \newcommand and/or \renewcommand 
% but it loads and uses file contents as the command.
% This solves problems with passing keywords to hypersetup
% command. (Using newcommand and input only sets the filename
% as keywords.)
\RequirePackage{catchfile}

% Define empty variables for these parameters
\newcommand{\LCIntTitle}{}
\newcommand{\LCIntAuthor}{}
\newcommand{\LCIntAuthorDegrees}{}
\newcommand{\LCIntUniversity}{}
\newcommand{\LCIntFaculty}{}
\newcommand{\LCIntDepartment}{}
\newcommand{\LCIntSupervisor}{}
\newcommand{\LCIntDegree}{}
\newcommand{\LCIntMonth}{}
\newcommand{\LCIntYear}{}
\newcommand{\LCIntSubject}{}
\newcommand{\LCIntAbstractEnFilename}{}
\newcommand{\LCIntAbstractCsFilename}{}
\newcommand{\LCIntKeywordsEnFilename}{}
\newcommand{\LCIntKeywordsEn}{}
\newcommand{\LCIntKeywordsCsFilename}{}
\newcommand{\LCIntCoverImageFilename}{}
\newcommand{\LCIntUniversityLogoFilename}{}
\newcommand{\LCIntUniversityLogoBWFilename}{}
\newcommand{\LCIntUniversityLogoNegFilename}{}
\newcommand{\LCIntAssignmentFilename}{}
\newcommand{\LCIntAcknowledgementsFilename}{}

% Define commands for setting  the above variables.
\newcommand{\LCSetTitle}[1]{\renewcommand{\LCIntTitle}{#1}\LCIntSetupPdfProps}
\newcommand{\LCSetAuthor}[1]{\renewcommand{\LCIntAuthor}{#1}\LCIntSetupPdfProps}
\newcommand{\LCSetAuthorDegrees}[1]{\renewcommand{\LCIntAuthorDegrees}{#1}}
\newcommand{\LCSetUniversity}[1]{\renewcommand{\LCIntUniversity}{#1}}
\newcommand{\LCSetFaculty}[1]{\renewcommand{\LCIntFaculty}{#1}}
\newcommand{\LCSetDepartment}[1]{\renewcommand{\LCIntDepartment}{#1}}
\newcommand{\LCSetSupervisor}[1]{\renewcommand{\LCIntSupervisor}{#1}}
\newcommand{\LCSetDegree}[1]{\renewcommand{\LCIntDegree}{#1}}
\newcommand{\LCSetMonth}[1]{\renewcommand{\LCIntMonth}{#1}}
\newcommand{\LCSetYear}[1]{\renewcommand{\LCIntYear}{#1}}
\newcommand{\LCSetSubject}[1]{\renewcommand{\LCIntSubject}{#1}\LCIntSetupPdfProps}
\newcommand{\LCAddAbstractEn}[1]{\renewcommand{\LCIntAbstractEnFilename}{#1}}
\newcommand{\LCAddAbstractCs}[1]{\renewcommand{\LCIntAbstractCsFilename}{#1}}
\newcommand{\LCAddKeywordsEn}[1]{
	\renewcommand{\LCIntKeywordsEnFilename}{#1}
	\CatchFileDef{\LCIntKeywordsEn}{#1}{}
	\LCIntSetupPdfProps
}
\newcommand{\LCAddKeywordsCs}[1]{\renewcommand{\LCIntKeywordsCsFilename}{#1}}
\newcommand{\LCAddCoverImage}[1]{\renewcommand{\LCIntCoverImageFilename}{#1}}
\newcommand{\LCAddUniversityLogo}[1]{\renewcommand{\LCIntUniversityLogoFilename}{#1}}
\newcommand{\LCAddUniversityLogoBW}[1]{\renewcommand{\LCIntUniversityLogoBWFilename}{#1}}
\newcommand{\LCAddUniversityLogoNeg}[1]{\renewcommand{\LCIntUniversityLogoNegFilename}{#1}}
\newcommand{\LCAddAssignment}[1]{\renewcommand{\LCIntAssignmentFilename}{#1}}
\newcommand{\LCAddAcknowledgements}[1]{\renewcommand{\LCIntAcknowledgementsFilename}{#1}}




% -----------------------------------------------------------------------------
% Front matter
% -----------------------------------------------------------------------------

% The frontmatter environment set the page numbering to lowercase roman for 
% ack, abstract, toc, lof, lot, loa, etc. It also resets page numbering for the 
% remainder of thesis (arabic, starting at 1).
\newenvironment{frontmatter}
{%
 	\setcounter{page}{1}
	\renewcommand{\thepage}{\roman{page}}
}
{%
	\clearpage
	\renewcommand{\thepage}{\arabic{page}}
	\setcounter{page}{1}
	\LCIntClearDoublePage
}




% -----------------------------------------------------------------------------
% Cover page
% -----------------------------------------------------------------------------

\RequirePackage{tikz}
\usetikzlibrary{calc}

\RequirePackage{afterpage}
%multi-row
\RequirePackage{multirow}
 

% Command that prints cover page with background filled with color
\newcommand{\LCPrintCoverPageBasic}{
	
	\LCIntClearDoublePage
		
	\newgeometry{margin=0pt}
    \thispagestyle{empty}
	\pagecolor{lc-dark-blue1}
	
	\vspace*{2cm}
	%\hspace*{2cm}
	\begin{table}[h!]
		\begin{tabular}{crl}%
			% Logo, Unievrsity, Faculty line
		    \multirow[b]{2}{2cm}[0cm]{} & %
		    \multirow[b]{2}{1.75cm}[0.025cm]{%
		    	\includegraphics[height=1.4cm]{\LCIntUniversityLogoNegFilename}%
		    } & {\huge\color{ctu-white}\LCIntUniversity} \\[0.25cm]
		    &&{\huge\color{ctu-white}\LCIntFaculty}
		\end{tabular}
	\end{table}
	
	\vspace*{3.5cm}
	\begin{center}
		{\LARGE\color{ctu-white}\LCIntDegree}
	\end{center}
	\vspace*{1cm}
	\begin{center}
		\parbox{0.75\linewidth}{\centering\Huge\color{ctu-white}\LCIntTitle}
	\end{center}
	\vspace*{2cm}
	\begin{center}
		\includegraphics[width=0.5\textwidth]{\LCIntCoverImageFilename}
	\end{center}
	\vfill
	\begin{center}
		{\huge\color{ctu-white}\LCIntAuthor}
	\end{center}
	\vspace*{2cm}
	
	% Restore page settings
	\restoregeometry
	\afterpage{\nopagecolor}
}


% Command that prints cover page with background filled with image
\newcommand{\LCPrintCoverPageFancy}{
	
	\LCIntClearDoublePage
		
	\newgeometry{margin=0pt}
    \thispagestyle{empty}
    
	\tikz[remember picture,overlay] \node[opacity=1,inner sep=0pt] at (current page.center){\includegraphics[width=\paperwidth,height=\paperheight]{\LCIntCoverImageFilename}};
	
	\vspace*{2cm}
	%\hspace*{2cm}
	\begin{table}[h!]
		\begin{tabular}{crl}%
			% Logo, Unievrsity, Faculty line
		    \multirow[b]{2}{2cm}[0cm]{} & %
		    \multirow[b]{2}{1.75cm}[0.025cm]{%
		    	\includegraphics[height=1.4cm]{\LCIntUniversityLogoNegFilename}%
		    } & {\huge\color{ctu-white}\LCIntUniversity} \\[0.25cm]
		    &&{\huge\color{ctu-white}\LCIntFaculty}
		\end{tabular}
	\end{table}
	
	\vspace*{3.5cm}
	\begin{center}
		{\LARGE\color{ctu-white}\LCIntDegree}
	\end{center}
	\vspace*{1cm}
	\begin{center}
		\parbox{0.75\linewidth}{\centering\Huge\color{ctu-white}\LCIntTitle}
	\end{center}
	\vspace*{2cm}
	
	\vfill
	\begin{center}
		{\huge\color{ctu-white}\LCIntAuthor}
	\end{center}
	\vspace*{2cm}
	
	% Restore page settings
	\restoregeometry
	\afterpage{\nopagecolor}
}



% -----------------------------------------------------------------------------
% Title page
% -----------------------------------------------------------------------------

\newcommand{\LCPrintTitlePage}{%
	\LCIntClearDoublePage
	\newgeometry{left=1.25in, right=1in, top=0.8in, bottom=1in}
	\thispagestyle{empty}
	\begin{center}
		\begin{figure*}[!t]
			\begin{subfigure}[]{2.45in}
				{\color{ctu-black}{\rule{2.45in}{0.3mm}}}
			\end{subfigure}%
			~
			\begin{subfigure}[]{1.0in}
				\includegraphics[width=\textwidth]{\LCIntUniversityLogoBWFilename}
			\end{subfigure}%
			~
			\begin{subfigure}[]{2.45in}
				{\color{ctu-black}{\rule{2.45in}{0.3mm}}}
			\end{subfigure}
		\end{figure*}
		\Large{\MakeUppercase{\LCIntUniversity}} \\
		\Large{{\LCIntFaculty}} \\
		\Large{{\LCIntDepartment}} \\
		%\vspace{1in} 
		\vfill
		
		\huge{\bf{{\LCIntTitle}}}
		
		\vspace{0.5in}
		
		\huge{{\LCIntDegree}}
		
		\vspace*{0.5in}
		
		\huge{{\LCIntAuthor}} 
		\vspace{0.5in}
		
		\Large{Supervisor} \\
		\Large{{\LCIntSupervisor}} \\
		
		\vfill
		
		\Large{\LCIntYear}\\
		{\color{ctu-black}{\rule{6.05in}{0.3mm}}}
	\end{center}
	
	\LCIntClearDoublePage
	\restoregeometry
}



% -----------------------------------------------------------------------------
% Thesis assignment
% -----------------------------------------------------------------------------

\RequirePackage{pdfpages}

\newcommand{\LCPrintAssignment}{
	\LCIntClearDoublePage
	\thispagestyle{plain}
	\includepdf[page={1}]{\LCIntAssignmentFilename}
}





% -----------------------------------------------------------------------------
% Declaration
% -----------------------------------------------------------------------------

% Define LCPrintDeclaration command
\newcommand{\LCPrintDeclaration}{
	\LCIntClearDoublePage
	\chapter*{Declaration}
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{Declaration}
	\markboth{Declaration}{Declaration} % Set header

	I hereby declare that this thesis is my original work and it has been written by me in its entirety. 
	I have duly acknowledged all the sources of information which have been used in the thesis. 
	This thesis has also not been submitted for any degree in any university previously.

	\vspace{0.5cm}
	\noindent	
	\LCIntAuthor \\
	\LCIntMonth\ \LCIntYear
}




% -----------------------------------------------------------------------------
% Acknowledgements
% -----------------------------------------------------------------------------

% Define LCPrintAbstract command
\newcommand{\LCPrintAcknowledgements}{
	\LCIntClearDoublePage
	\chapter*{Acknowledgements}
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{Acknowledgements}	
	\markboth{Acknowledgements}{Acknowledgements} % Set header
	\input{\LCIntAcknowledgementsFilename}
}




% -----------------------------------------------------------------------------
% English and czech abstract
% -----------------------------------------------------------------------------

\newcommand{\LCPrintAbstract}{
	\LCIntClearDoublePage
	\chapter*{Abstract}
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{Abstract}
	\markboth{Abstract}{Abstract} % Set header
	\vspace{-20pt}
	\input{\LCIntAbstractEnFilename}
	
	\textbf{Keywords:} \input{\LCIntKeywordsEnFilename}	
	
	{
	\let\clearpage\relax
	\vspace{50pt}	
	\chapter*{Abstrakt}
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{Abstrakt}
	\markboth{Abstrakt}{Abstrakt} %Set header
	\vspace{-20pt}
	\input{\LCIntAbstractCsFilename}
		
	\textbf{Klíčová slova:} \input{\LCIntKeywordsCsFilename}	
	}
}




% -----------------------------------------------------------------------------
% TOC (table of contents)
% -----------------------------------------------------------------------------

% Using tocloft, the toc can be formatted easily.
% Loaded above before parskip.
% \RequirePackage[titles, subfigure]{tocloft}

% Remove dot if you want
% \renewcommand{\cftdotsep}{\cftnodots}

% Format chapter entries differently in toc.
\renewcommand{\cftchapfont}{\bfseries}

% Create custom colour settings
%\expandafter\def\csname authoredby#1\endcsname{
%    \renewcommand{\cftparfont}{\color{\ctu-dark-blue}} % Subsection colour
%}

% Define LCAddToc command
\newcommand{\LCPrintToc}{
	\LCIntClearDoublePage
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{Contents}
	\tableofcontents
	\markboth{Contents}{Contents} % Set header
	\LCIntClearDoublePage
}




% -----------------------------------------------------------------------------
% LOF, LOT, LOA (list of figures, tables, algorithms)
% -----------------------------------------------------------------------------

% Remove dots from list of algorithms.
% This is necessary because we use algorithm2e which mananges its own list of algorithms.
%\makeatletter
%	\renewcommand{\@dotsep}{5000}
%\makeatother

% Fix the indentation of figure and table entries in the lof, lot, and loa.
\setlength{\cftfigindent}{0in}
\setlength{\cfttabindent}{0in}

\newcommand{\LCPrintLof}{%
	\LCIntClearDoublePage
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{List of Figures}
	\markboth{List of Figures}{List of Figures} % Set header
	\listoffigures
	\LCIntClearDoublePage
}
\newcommand{\LCPrintLot}{%
	\LCIntClearDoublePage
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{List of Tables}
	\markboth{List of Tables}{List of Tables} % Set header
	\listoftables
	\LCIntClearDoublePage
}
\newcommand{\LCPrintLoa}{%
	\LCIntClearDoublePage
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{List of Algorithms}
	\markboth{List of Algorithms}{List of Algorithms} % Set header
	\listofalgorithmes % Note an extra e, it is required because we use algorithm2e.
	\LCIntClearDoublePage
}




% -----------------------------------------------------------------------------
% Appendix
% -----------------------------------------------------------------------------

% Use appendix package without page with Appendices title
\RequirePackage[header]{appendix}
% Use appendix package with page with Appendices title
%\RequirePackage[toc,page]{appendix}

% Change text color of the appendix page (the first page containing title Appendices)
%\let\appendixpagenameorig\appendixpagename
%\renewcommand{\appendixpagename}{\color{ctu-black}\appendixpagenameorig}




% -----------------------------------------------------------------------------
% Nomenclature
% -----------------------------------------------------------------------------

% Use package for long and mulit-page tables
\RequirePackage{longtable}

% Use package for glossary/nomenclature and list of abbreviations
\RequirePackage[nomain, acronym, style=super, nonumberlist, toc]{glossaries}

% Do not use hyperlinks when using glossaries 
\glsdisablehyper

% Define symbol list as a glossary domain
\newglossary[slg]{listofsymbols}{syi}{syg}{List of Symbols}

% Set width of the glossary table
\setlength{\glsdescwidth}{\linewidth}

% Define cutomized glossary style for list of symbols
\newglossarystyle{LClosStyle}{
	% Base this style on the list style
	\setglossarystyle{list}
	% Change the table type
	\renewenvironment{theglossary}
  		{\begin{longtable}[l]{cp{1\glsdescwidth}}}
  		{\end{longtable}}
  	% Change the table header
	\renewcommand*{\glossaryheader}{
  		\bf{Symbol} & \bf{Description} \vspace{2.5mm} %\hline
  		\endhead
  	}
  	%  Change the displayed items
	\renewcommand*{\glossentry}[2]{
		\glstarget{##1}{\glossentryname{##1}} & \glossentrydesc{##1}
		\tabularnewline
	}
}

% Make glossaries groups (acronyms and list of symbols) 
\makeglossaries

% Load acronyms from given file
\newcommand{\LCAddAcronyms}[1]{\input{#1}}

% Load list of symbols from given file
\newcommand{\LCAddListOfSymbols}[1]{\input{#1}}

% Print acronyms
\newcommand{\LCPrintAcronyms}{\printglossary[type=\acronymtype, nopostdot, title=Acronyms]}

% Print list of symbols
\newcommand{\LCPrintListOfSymbols}{
	% Add all used symbols to list of symbols
	\glsaddall[types={listofsymbols}]
	{
		\renewcommand{\arraystretch}{1.25}
		\printglossary[type=listofsymbols, nopostdot, style=LClosStyle, title=List of Symbols]
	}
}




% -----------------------------------------------------------------------------
% Bibliography
% -----------------------------------------------------------------------------

% Repair command cite. The original version contains (by mistake) a space at
% the end which causes a punctuation sign follofing a citation to be placed on
% new line in some cases.

% Set bibliography style using BibLaTeX
\ifthenelse{\equal{\LCIntBibStyle}{basic}} {
	\RequirePackage[backend=biber,sorting=none, citestyle=ieee]{biblatex}
}{
	\RequirePackage[backend=biber,sorting=none, citestyle=ieee, defernumbers=true]{biblatex}
}

% Load bibliography from given file
\newcommand{\LCAddBibliography}[1]{\addbibresource{#1}}


% Print bibliography command
\newcommand{\LCPrintBibliography}{
	\chapter*{Bibliography}
	\phantomsection % Requires hyperref; this is to fix the link.
	\addcontentsline{toc}{chapter}{Bibliography}
	\markboth{Bibliography}{Bibliography} % Set header
	
	\ifthenelse{\equal{\LCIntBibStyle}{basic}} {
		\printbibliography[heading=none]
	}{
		\defbibfilter{books} {
 			type=book or
  			type=mvbook or
  			type=inbook
		}
		\defbibfilter{papers} {
 			type=article or
  			type=periodical or
  			type=inproceedings or
  			type=proceedings or
  			type=mvproceedings
		}
		\defbibfilter{theses} {
 			type=thesis or
  			type=PhdThesis or
  			type=MastersThesis
		}
		\defbibfilter{reports_and_manuals} {
 			type=manual or
  			type=report
		}
		\defbibfilter{others} {
 			type=booklet or
  			type=collection or
  			type=mvcollection or
  			type=incollection or
  			type=misc or
  			type=online or
  			type=patent or
  			type=Unpublished
		}
		\printbibliography[filter=books,heading=subbibliography,title={Books}]
		\printbibliography[filter=papers,heading=subbibliography,title={Papers}]
		\printbibliography[filter=theses,heading=subbibliography,title={Theses}]
		\printbibliography[filter=reports_and_manuals,heading=subbibliography,title={Technical Reports and Manuals}]
		\printbibliography[filter=others,heading=subbibliography,title={Other References}]
	}
	
	\LCIntClearDoublePage
}




% -----------------------------------------------------------------------------
% Include packages for manipulation with figures
% -----------------------------------------------------------------------------

% Package that improves the interface for defining floating objects such as 
% figures and tables.
% Must be done before hyperref.
%\RequirePackage{float}     

% Package for inserting images (includegraphics command)
\RequirePackage{graphicx}

% Package for setting captions of subfigures
%\RequirePackage{subcaption}
\RequirePackage[labelformat=simple]{subcaption}

% Add parentheses around subfigure references (subref)
\renewcommand\thesubfigure{(\alph{subfigure})}
     

% Package for converting eps images to pdf
\RequirePackage{epstopdf}




% -----------------------------------------------------------------------------
% Include mathematical packages
% -----------------------------------------------------------------------------

% Most of the mathematical features are in this package.
\RequirePackage{amsmath}

% Package that extends amsmath and fixes some issues of amsmath.
\RequirePackage{mathtools}

% Package containing mathematical symbols and styles
% see: http://milde.users.sourceforge.net/LUCR/Math/mathpackages/amssymb-symbols.pdf
\RequirePackage{amssymb}

% Package similar to anssymb
\RequirePackage{dsfont}

% Package for defining mathematical theorems
\RequirePackage{amsthm}

% Package containing better typestting enviroment for some physical symbols.
\RequirePackage{mathrsfs}

% Use package for easy use of SI units in the text
\RequirePackage{siunitx}

% Use package for bold mathematical symbols
\RequirePackage{bm}

% Use package for upright (not italic) greek letters
\RequirePackage{upgreek}

% Use package to calculate width of symbols
\RequirePackage{calc}

% Use package defining abs and norm commands
\RequirePackage{commath}




% -----------------------------------------------------------------------------
% Better format of references (clevref)
% -----------------------------------------------------------------------------

% Must come as late as possible, especially after hyperref.
\RequirePackage[capitalize]{cleveref}

% Disable the automatic abbreviations of equations and figures.
\crefname{equation}{Equation}{Equations}
\crefname{figure}{Figure}{Figures}
\Crefname{equation}{Equation}{Equations}
\Crefname{figure}{Figure}{Figures}

% Change the way links are produced in PDF documents.
\crefformat{chapter}{#2Chapter~#1#3}
\crefformat{section}{#2Section~#1#3}
\crefformat{figure}{#2Figure~#1#3}
\crefformat{equation}{#2Equation~#1#3}
\crefformat{table}{#2Table~#1#3}
\Crefformat{chapter}{#2Chapter~#1#3}
\Crefformat{section}{#2Section~#1#3}
\Crefformat{figure}{#2Figure~#1#3}
\Crefformat{equation}{#2Equation~#1#3}
\Crefformat{table}{#2Table~#1#3}
\creflabelformat{equation}{#2#1#3}




% -----------------------------------------------------------------------------
% Create custom mathematical commands
% -----------------------------------------------------------------------------

% Define distance between matrix rows
\renewcommand*{\arraystretch}{1.25}

% Define width of equal sign
\newlength{\equalsignwidth}
\setlength{\equalsignwidth}{\widthof{$=$}}

% Define custom equal sign in math mode with spaces
\newcommand{\meq}{\; &= \;} 
\newcommand{\meqskip}{\; &\hspace{\equalsignwidth} \;}
\newcommand{\meqna}{\; = \;}
\newcommand{\mapprox}{\; &\approx \;}
\newcommand{\mleq}{\; &\leq \;} 

% Define double tilde command
%\usepackage{accents}
%\newcommand{\dbtilde}[1]{\accentset{\approx}{#1}}
\newcommand{\dtilde}[1]{\tilde{\raisebox{-0.5pt}[0.85\height]{$\tilde{#1}$}}}

% Custom size overline/bar above symbols
\newcommand{\xbar}[2][3]{%
{}\mkern#1mu\overline{\mkern-#1mu#2}}

% Brackets
\newcommand{\braRound}[1]{\ensuremath{\left(#1\right)}}
\newcommand{\braSquare}[1]{\ensuremath{\left[#1\right]}}
\newcommand{\braCurl}[1]{\ensuremath{\left\lbrace#1\right\rbrace}}
\newcommand{\braAngle}[1]{\ensuremath{\left\langle#1\right\rangle}}

% Differential 
\newcommand{\diff}{\textrm{d}}

% Derivative
\newcommand{\derivative}[2]{\ensuremath{\frac{\diff#1}{\diff#2}}}
\newcommand{\pderivative}[2]{\ensuremath{\frac{\partial#1}{\partial#2}}}

% Transposition
\newcommand{\transpose}{\ensuremath{^{\textrm{T}}}}
\newcommand{\itranspose}{\ensuremath{^{-\textrm{T}}}}

% Unit step
\newcommand{\unitstep}{\ensuremath{\mathds{1}}}

% Controllability matrix
\newcommand{\cntmat}{\ensuremath{\mathcal{C}}}

% Observability matrix
\newcommand{\obsmat}{\ensuremath{\mathcal{O}}}

% Range of map
\newcommand{\Range}{\textrm{Range}}

% Span of map
\newcommand{\Span}{\textrm{Span}}

% Rank of matrix
\newcommand{\rank}[1]{\textrm{rank}}

% Eigenvalue
\newcommand{\eig}[1]{\textrm{eig}}

% Arg max
\newcommand{\argmax}[1]{\ensuremath{\textrm{arg}\ \max\limits_{#1}}}

% Algebraic multiplicity
\newcommand{\algmult}{\ensuremath{\textrm{n}_{\textrm{a}}}}

% Geometrical multiplicity
\newcommand{\geomult}{\ensuremath{\textrm{n}_{\textrm{g}}}}

% Set of natural numbers
\newcommand{\numsetN}{\ensuremath{\mathbb{N}}}

% Set of integer numbers
\newcommand{\numsetZ}{\ensuremath{\mathbb{Z}}}

% Set of real numbers
\newcommand{\numsetR}{\ensuremath{\mathbb{R}}}

% Set of complex numbers
\newcommand{\numsetC}{\ensuremath{\mathbb{C}}}

% Custom SI units
\DeclareSIUnit \rpm {RPM} % rotates per minute
\DeclareSIUnit \Arms {A_{\textrm{(rms)}}}   % RMS Ampere
\DeclareSIUnit \Apeak {A_{\textrm{(peak)}}} % Peak Ampere
\DeclareSIUnit \Vrms {V_{\textrm{(rms)}}}   % RMS Volt
\DeclareSIUnit \Vpeak {V_{\textrm{(peak)}}} % Peak Volt

% Custom US units
\let\DeclareUSUnit\DeclareSIUnit
\let\US\SI
\DeclareUSUnit\inch{in}    % inch
\DeclareUSUnit\ounce{oz}   % ounce
\DeclareUSUnit\ouncef{ozf} % ounce force

% -----------------------------------------------------------------------------
% Others
% -----------------------------------------------------------------------------

% Todo notes highlighted in the text (to highlight which parts
% of the document are to be changed or checked.)
\RequirePackage[textsize = footnotesize, textwidth=17mm]{todonotes}




