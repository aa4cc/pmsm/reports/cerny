
%% Clear workspace
% clear all
close all
clc


%% Plot 1 - plot circle

% Current limit
Im = 3.67;

% Create circle
ang = 0:0.01:2*pi;
x_circle = Im*cos(ang);
y_circle = Im*sin(ang);

% Plot circle
figure;
hold on
axis equal
plot(x_circle, y_circle);


%% Plot 2 - plot octagon
f1 = @(x) 1/(sqrt(2)-1)*(Im - x);
f2 = @(x) Im - (sqrt(2)-1)*x;
f3 = @(x) Im + (sqrt(2)-1)*x;
f4= @(x) 1/(sqrt(2)-1)*(Im + x);
f5= @(x) 1/(sqrt(2)-1)*(-Im - x);
f6 = @(x) -Im - (sqrt(2)-1)*x;
f7 = @(x) -Im + (sqrt(2)-1)*x;
f8 = @(x) 1/(sqrt(2)-1)*(-Im + x);

i_alpha_1 = Im;
i_alpha_2 = Im/sqrt(2);
i_alpha_3 = 0;
i_alpha_4 = -Im/sqrt(2);
i_alpha_5 = -Im;

line1 = [    i_alpha_1,     i_alpha_2;
         f1(i_alpha_1), f1(i_alpha_2)];
line2 = [    i_alpha_2,     i_alpha_3;
         f2(i_alpha_2), f2(i_alpha_3)];
line3 = [    i_alpha_3,     i_alpha_4;
         f3(i_alpha_3), f3(i_alpha_4)];
line4 = [    i_alpha_4,     i_alpha_4;
         f4(i_alpha_4), f4(i_alpha_4)];
line5 = [    i_alpha_5,     i_alpha_4;
         f5(i_alpha_5), f5(i_alpha_4)];
line6 = [    i_alpha_4,     i_alpha_3;
         f6(i_alpha_4), f6(i_alpha_3)];
line7 = [    i_alpha_3,     i_alpha_2;
         f7(i_alpha_3), f7(i_alpha_2)];
line8 = [    i_alpha_2,     i_alpha_1;
         f8(i_alpha_2), f8(i_alpha_1)];
border = [line1, line2, line3, line4, line5, line6, line7, line8];

%figure;
hold on
axis equal

% plot circle
plot(x_circle, y_circle);

% Plot octagon
plot(border(1, :), border(2, :), 'g');


%% Plot 3 - reformulated (to check that it is correct)

% Define functions describing the octagon
f1 = @(d, q) abs(d + (sqrt(2)-1)*q);
f2 = @(d, q) abs(d - (sqrt(2)-1)*q);
f3 = @(d, q) abs((sqrt(2)-1)*d + q);
f4 = @(d, q) abs((sqrt(2)-1)*d - q);

% Phase current limit
Im = 3.67;

% Grid the DQ current space
[id, iq] = meshgrid(-1.2*Im:.1:1.2*Im);
id = id(:)';
iq = iq(:)';

% Remove points that do not satisfy the conditions
indices = [];
indices = [indices, find(f1(id, iq) > Im)];
indices = [indices, find(f2(id, iq) > Im)];
indices = [indices, find(f3(id, iq) > Im)];
indices = [indices, find(f4(id, iq) > Im)];
indices = unique(indices);
id(indices) = [];
iq(indices) = [];

% Create figure
figure;
set_figure
hold on
axis equal

% plot circle
plot(x_circle, y_circle);

% Plot octagon
plot(id, iq, '.');


%% Plot 4 - reformulated (to check that it is correct)

% Phase current limit
Im = 3.67;

% Define matrices describing the octagon
P = [          +1, +sqrt(2)-1;
               +1, -sqrt(2)+1;
               -1, +sqrt(2)-1;
               -1, -sqrt(2)+1;
       +sqrt(2)-1,         +1;
       +sqrt(2)-1,         -1;
       -sqrt(2)+1,         +1;
       -sqrt(2)+1,         -1];
p = [Im; Im; Im; Im; Im; Im; Im; Im];
 
% Grid the DQ current space
[id, iq] = meshgrid(-1.2*Im:.1:1.2*Im);
id = id(:)';
iq = iq(:)';

% Remove points that do not satisfy the conditions
indices = find(sum(P*[id; iq] > p));
id(indices) = [];
iq(indices) = [];

% Create figure
figure;
set_figure
hold on
axis equal

% plot circle
plot(x_circle, y_circle);

% Plot octagon
plot(id, iq, '.');


%% Plot 5 - filled feasible region
hfig = figure;
set_figure();
hold on
axis equal
patch('Faces', 1:length(border), 'Vertices', border', 'EdgeColor', 'g', 'FaceColor', 'g', 'FaceAlpha', 0.1);
plot(x_circle, y_circle, '--b');
hleg = legend('App. feasible region', '$i_{\textrm{d}}^2 + i_{\textrm{q}}^2 = I_{\textrm{max}}^2$', 'Location', 'northeastoutside');
xlabel('$i_{\textrm{d}}$ [A]');
ylabel('$i_{\textrm{q}}$ [A]');

% Enlarge figure width by the width of the legend box
enlarge_fig(hfig, hleg);

% Set y-axis limits
ylim([-4.5, 4.5])

% Set numbers on axes
set(gca,'xtick', [-6:2:6]);
set(gca,'ytick', [-6:2:6]);


%% Save plot 5

% Save
% file_name = 'constraints3.pdf';
% saveas(gca, file_name);
% system(['pdfcrop ', file_name, ' ', file_name]);


%% Save to border_current_alpha_beta
border_current_dq = border;
circle_x_current_dq = x_circle;
circle_y_current_dq = y_circle;

