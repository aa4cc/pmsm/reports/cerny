
%% Clear workspace
% clear all
close all
clc


%% Plot 1 - plot circle

% Inverted DC-bus voltage
Vdc = 24;

% Phase voltage limit
Vm = Vdc/sqrt(3);

% Create circle
ang = 0:0.01:2*pi;
x_circle = Vm*cos(ang);
y_circle = Vm*sin(ang);

% Plot circle
figure;
hold on
axis equal
plot(x_circle, y_circle);


%% Plot 2 - plot octagon
f1 = @(x) 1/(sqrt(2)-1)*(Vm - x);
f2 = @(x) Vm - (sqrt(2)-1)*x;
f3 = @(x) Vm + (sqrt(2)-1)*x;
f4= @(x) 1/(sqrt(2)-1)*(Vm + x);
f5= @(x) 1/(sqrt(2)-1)*(-Vm - x);
f6 = @(x) -Vm - (sqrt(2)-1)*x;
f7 = @(x) -Vm + (sqrt(2)-1)*x;
f8 = @(x) 1/(sqrt(2)-1)*(-Vm + x);

i_alpha_1 = Vm;
i_alpha_2 = Vm/sqrt(2);
i_alpha_3 = 0;
i_alpha_4 = -Vm/sqrt(2);
i_alpha_5 = -Vm;

line1 = [    i_alpha_1,     i_alpha_2;
         f1(i_alpha_1), f1(i_alpha_2)];
line2 = [    i_alpha_2,     i_alpha_3;
         f2(i_alpha_2), f2(i_alpha_3)];
line3 = [    i_alpha_3,     i_alpha_4;
         f3(i_alpha_3), f3(i_alpha_4)];
line4 = [    i_alpha_4,     i_alpha_4;
         f4(i_alpha_4), f4(i_alpha_4)];
line5 = [    i_alpha_5,     i_alpha_4;
         f5(i_alpha_5), f5(i_alpha_4)];
line6 = [    i_alpha_4,     i_alpha_3;
         f6(i_alpha_4), f6(i_alpha_3)];
line7 = [    i_alpha_3,     i_alpha_2;
         f7(i_alpha_3), f7(i_alpha_2)];
line8 = [    i_alpha_2,     i_alpha_1;
         f8(i_alpha_2), f8(i_alpha_1)];
border = [line1, line2, line3, line4, line5, line6, line7, line8];

%my_figure;
hold on
axis equal

% plot circle
plot(x_circle, y_circle);

% Plot octagon
plot(border(1, :), border(2, :), 'g');


%% Plot 3 - filled feasible region
hfig = figure;
set_figure();
hold on
axis equal
patch('Faces', 1:length(border), 'Vertices', border', 'EdgeColor', 'g', 'FaceColor', 'g', 'FaceAlpha', 0.1);
plot(x_circle, y_circle, '--b');
hleg = legend('App. feasible region', '$v_{\textrm{d}}^2 + v_{\textrm{q}}^2 = V_{\textrm{max}}^2$', 'Location', 'northeastoutside');
xlabel('$v_{\textrm{d}}$ [V]');
ylabel('$v_{\textrm{q}}$ [V]');

% Enlarge figure width by the width of the legend box
enlarge_fig(hfig, hleg);

% Set y-axis limits
ylim([-3/4*Vdc-0.5, +3/4*Vdc+0.5]);

% Set numbers on axes
set(gca,'xtick', [-20:5:20]);
set(gca,'ytick', [-20:5:20]);

%% Save plot 3

% Save
% file_name = 'constraints4.pdf';
% saveas(gca, file_name);
% system(['pdfcrop ', file_name, ' ', file_name]);


%% Save to border_voltage_alpha_beta
border_voltage_dq = border;
circle_x_voltage_dq = x_circle;
circle_y_voltage_dq = y_circle;


