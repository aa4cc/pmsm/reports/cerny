:: Iterate over *.svg files in subfolders (in depth 1)
@echo OFF
setlocal enabledelayedexpansion
for /d %%i in (*) do (
  for %%j in (%%i\*.svg) do (
    set inputf=%%~pnj.svg
    set outputf=%%~pnj.pdf
    echo Converting !inputf!
    inkscape --export-area-drawing --export-type="pdf" !inputf!
  )
)
pause
