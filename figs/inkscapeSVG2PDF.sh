#!/bin/bash

# Iterate over *.svg files in subfolders (in depth 1)
FILES=./*/*.svg
for f in $FILES; do
  echo "Converting $f"
  g="${f%svg}pdf"
  inkscape -f "$f"  --export-area-drawing  --export-pdf="$g"
done
