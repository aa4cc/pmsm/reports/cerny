# Requirements -----------------------------------------------------------------
# The LCThesis class was tested and works under linux using TeX Live.
# It can be, however, compiled under Windows using MiKTeX but some more effort
# needs to be made. Specifically, some font packages need to be installed (these
# might not install automatically, instead they cause an error). Also glossaries
# package needs a pearl engine to be installed, e.g. ActivePearl.

# Equivalent TEXMAKER user commands --------------------------------------------
# Instead of running this Makefile you can define user command in TEXMAKER and
# call it on thesis.tex file.
# (The following commands are for Linux + TeX Live. For Windows, you need to
# use different pdf viewer. To do so, replace evince with e.g.
# "C:/Program Files/Adobe/Reader 11.0/Reader/AcroRd32.exe")
# Equivalent TEXMAKER user command with "use build subdirectory" ticked:
#		pdflatex -synctex=1 -interaction=nonstopmode %.tex|biber -output-directory=build %|makeglossaries -d build %|pdflatex -synctex=1 -interaction=nonstopmode %.tex|pdflatex -synctex=1 -interaction=nonstopmode %.tex|cp ./build/%.pdf %.pdf|evince %.pdf
# Equivalent TEXMAKER user command without "use build subdirectory" ticked:
#		pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build %.tex|biber -output-directory=build %|makeglossaries -d build %|pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build %.tex|pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build %.tex|cp ./build/%.pdf %.pdf|evince %.pdf

# Define options ---------------------------------------------------------------
# Define latex compiler
LATEX_COMPILER := pdflatex
# Define bibliography compiler
BIB_COMPILER := biber
# Define index compiler
INDEX_COMPILER := makeglossaries
# Define file to compile (without .tex extension)
TARGET_FILE := thesis
# Define build folder
BUILD_FOLDER := build
# Define verbose (TRUE or FALSE)
#VERBOSE := TRUE
VERBOSE := FALSE


# Define auxiliary variables ---------------------------------------------------
TARGET_FILE_FULL := ./$(TARGET_FILE).tex
BUILD_FOLDER_FULL := ./$(BUILD_FOLDER)


# Define auxiliary functions ---------------------------------------------------
define create_build_folder
	@# Argument 1: build folder
	@# Create BUILD_FOLDER if it does not exist
	@mkdir -p $(1)
endef

define set_text_color_to_red
	@echo -n "\033[0;31m"
endef

define set_text_color_to_green
	@echo -n "\033[0;32m"
endef

define set_text_color_to_blue
	@echo -n "\033[0;34m"
endef

define set_text_color_to_cyan
	@echo -n "\033[0;36m"
endef

define set_text_color_to_brown
	@echo -n "\033[0;33m"
endef

define set_text_color_to_default
	@echo -n "\033[0m"
endef

define run_latex_verbose
	@# Argument 1: latex compiler
	@# Argument 2: target file
	@# Argument 3: build folder
	@$(1) -output-directory=$(3) $(2)
endef

define run_latex_quiet
	@# Argument 1: latex compiler
	@# Argument 2: target file
	@# Argument 3: build folder
	@$(1) -output-directory=$(3) $(2) > $(3)/tmp.log
endef

define run_latex
	@# Argument 1: latex compiler
	@# Argument 2: target file
	@# Argument 3: build folder
	@# Argument 4: verbose
	$(if $(filter $(4),TRUE),$(call run_latex_verbose,$(1),$(2),$(3)),$(call run_latex_quiet,$(1),$(2),$(3)))
endef

define run_makeindex_verbose
	@# Argument 1: index compiler
	@# Argument 2: target file
	@# Argument 3: build folder
	@$(1) -d $(3) $(2)
endef

define run_makeindex_quiet
	@# Argument 1: index compiler
	@# Argument 2: target file
	@# Argument 3: build folder
	@$(1) -q -d $(3) $(2)
endef

define run_makeindex
	@# Argument 1: index compiler
	@# Argument 2: target file
	@# Argument 3: build folder
	@# Argument 4: verbose
	$(if $(filter $(4),TRUE),$(call run_makeindex_verbose,$(1),$(2),$(3)),$(call run_makeindex_quiet,$(1),$(2),$(3)))
endef


# Define tasks -----------------------------------------------------------------
all:
	@echo "Step 1/5 - $(LATEX_COMPILER)"
	@$(MAKE) -s -C . pdf

	@echo "Step 2/5 - $(BIB_COMPILER)"
	@$(MAKE) -s -C . bib

	@echo "Step 3/5 - $(INDEX_COMPILER)"
	@$(MAKE) -s -C . nom

	@echo "Step 4/5 - $(LATEX_COMPILER)"
	@$(MAKE) -s -C . pdf

	@echo "Step 5/5 - $(LATEX_COMPILER)"
	@$(MAKE) -s -C . pdf

	@echo "Finished."

# This task does the same thing as all but it highlits the make progress
# by colors using escape sequences. These are not supported in all terminals.
# Thus, the default all task does not uses the highliting.
all2:
	$(set_text_color_to_cyan)
	@echo "Step 1/5 - $(LATEX_COMPILER)"
	$(set_text_color_to_default)
	@$(MAKE) -s -C . pdf

	$(set_text_color_to_brown)
	@echo "Step 2/5 - $(BIB_COMPILER)"
	$(set_text_color_to_default)
	@$(MAKE) -s -C . bib

	$(set_text_color_to_blue)
	@echo "Step 3/5 - $(INDEX_COMPILER)"
	$(set_text_color_to_default)
	@$(MAKE) -s -C . nom

	$(set_text_color_to_cyan)
	@echo "Step 4/5 - $(LATEX_COMPILER)"
	$(set_text_color_to_default)
	@$(MAKE) -s -C . pdf

	$(set_text_color_to_cyan)
	@echo "Step 5/5 - $(LATEX_COMPILER)"
	$(set_text_color_to_default)
	@$(MAKE) -s -C . pdf

	$(set_text_color_to_green)
	@echo "Finished."
	$(set_text_color_to_default)

pdf:
	$(call create_build_folder,$(BUILD_FOLDER_FULL))
	$(call run_latex,$(LATEX_COMPILER) -interaction=nonstopmode -halt-on-error,$(TARGET_FILE_FULL),$(BUILD_FOLDER_FULL),$(VERBOSE))
	@mv $(BUILD_FOLDER_FULL)/$(TARGET_FILE).pdf ./$(TARGET_FILE).pdf

bib:
	$(call create_build_folder,$(BUILD_FOLDER_FULL))
	$(call run_latex,$(BIB_COMPILER),$(TARGET_FILE),$(BUILD_FOLDER_FULL),$(VERBOSE))

nom:
	$(call create_build_folder,$(BUILD_FOLDER_FULL))
	$(call run_makeindex,$(INDEX_COMPILER),$(TARGET_FILE),$(BUILD_FOLDER_FULL),$(VERBOSE))

clean c:
	$(RM) -rv $(BUILD_FOLDER_FULL)/*

cleanall ca: clean
	$(RM) -rv $(TARGET_FILE).pdf $(BUILD_FOLDER_FULL)
