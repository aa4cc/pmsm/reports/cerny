# Master thesis by Lukas Cerny
Master thesis by Lukáš Černý.

# How to create pdf file
You can call make in command line to create pdf file.
The make command will do complete build including bibliography and glossaries.
See comments in Makefile to do the same thing in TEXMAKER.
