\chapter{Simulation\label{chap:Simulation}}

This chapter evaluates the control algorithms in simulation.
The continuous-control-set algorithms were combined with symmetrical \gls{pwm} with \gls{svm} equivalent common-mode voltage injection.
Other nonidealities, such as measurement noise and model uncertainty, were not considered.
The reference is assumed to be known in advance and the controllers may anticipate it.


\section{Chosen Parameters}

The sampling frequencies and prediction horizons were chosen such that the controllers fit the capabilities of the platform chosen for hardware evaluation. 
Other tuning parameters of the control algorithms were chosen such that the controllers give similar steady-state torque ripple.
The exact values are not of great importance because the goal of this chapter is not to find the best controller, but rather to describe the main characteristics.

\subsection{PI Controller}
The current loop of the \gls{pi} controller was designed to operate at $T_{\textrm{s1}} = \SI{25}{\kilo\hertz}$ the the proportional constant was tuned to \SI{20.3} and the integral constant was tuned to $0.95T_{\textrm{s1}}$ (the units of the constants are omitted).
The velocity controller was designed to operate at $T_{\textrm{s2}} = \SI{1}{\kilo\hertz}$ with  the proportional constant $1.2$ and the integral constant $0.04T_{\textrm{s2}}$.
The position controller operates at \SI{100}{\hertz} the proportional constant 50 and the integral constant 0 (the integral action is not necessary because the position is the integral of velocity).

\subsection{\gls{ccsmpc}}
Single-loop \gls{ccsmpc} was chosen to operate at \SI{25}{\kilo\hertz} and 
two lengths 5 and 10 of the prediction horizon were tested.
In the cascaded structure, the inner loop operated at \SI{25}{\kilo\hertz} and the outer one at \SI{1}{\kilo\hertz}.

\subsection{\gls{fcsmpc}}
The evaluation was done with the sampling frequency of \SI{500}{\kilo\hertz} and prediction horizon 3.


\section{Current Control}
Tracking of reference current was tested at velocity of \SI{307}{\radian\per\second} (\SI{2900}{\rpm}) by commanding  the direct current to zero and the quadrature current from steady-state value of \SI{2}{A} to \SI{3}{A}.
The steady state current compensated for nonzero load torque of \SI{-0.065}{\newton\meter}. 
Also, to find the effects of the external torque on the current controllers, a step change of the load torque from  \SI{-0.065} to  \SI{-0.2} was introduced. 
The results in fig. \ref{fig:DQcurrentscontrol} show that all three algorithms control the DQ currents as required --- the qudrature current tracks the reference and the direct current stays close to zero.



Even though \gls{pi} controller is very simple to implement, it provides very good results when properly tuned.
The main drawback is that the constrained are not properly imposed.
The choice of the relaxed constraints is justified here by the control of the direct current close to zero.


The anticipation of the reference is apparent in the response of \gls{ccsmpc}.
The small overshoot could be eliminated by choosing longer prediction horizon.
However, with short prediction horizon, it is difficult to completely remove the overshoot.

\gls{fcsmpc} also anticipates the reference but on much smaller time interval because it operates at much higher sampling rate.
It has also the fastest response to the step-change in reference.
The current ripple is not uniform, which results in wider spectrum of frequencies in the phase currents.
This can be fixed by choosing longer prediction horizon.
Doing so results in more uniform torque ripple.
Another interesting result is that the number of nodes \gls{fcsmpc} visit during the search for optimal control is greater during transients and small during steady state, see  fig. \ref{fig:visited_nodes}.


The step change in the load torque happened at time \SI{1e-3} and it had no direct effect on the DQ currents.
However, it indirectly influences them by increasing or decreasing the angular velocity.


% Figure
\begin{figure}[tb]
	%\begin{center}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/pid_q_current.pdf}
  			\subcaption{\gls{pi} control of $i_{\textrm{q}}$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/ccsmpc_q_current.pdf}
  			\subcaption{\gls{ccsmpc} control of $i_{\textrm{q}}$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/torque_tracking/fcsmpc_q_current.pdf}
  			\subcaption{\gls{fcsmpc} control of $i_{\textrm{q}}$.}
		\end{subfigure}
		\par\bigskip
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/pid_d_current.pdf}
  			\subcaption{\gls{pi} control of $i_{\textrm{d}}$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/ccsmpc_d_current.pdf}
  			\subcaption{\gls{ccsmpc} control of $i_{\textrm{d}}$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/torque_tracking/fcsmpc_d_current.pdf}
  			\subcaption{\gls{fcsmpc} control of $i_{\textrm{d}}$.}
		\end{subfigure}
	  	\caption{Control of the DQ currents. \label{fig:DQcurrentscontrol}}
	%\end{center}
\end{figure}


\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.3\textwidth]{figs/torque_tracking/fcsmpc_number_of_visited.pdf}
		\caption{The number of nodes visited by \gls{fcsmpc}\label{fig:visited_nodes}.}
	\end{center}
\end{figure}





\section{Velocity Control}

Reference velocity tracking was tested by setting the reference velocity to \SI{10}{\radian\per\second} (\SI{95}{\rpm}) from zero state.
A step-change of the external load torque from 0 to \SI{-0.05}{\newton\meter} was applied.
It turned out to be perhaps impossible to tune the \gls{fcsmpc} for velocity tracking and all attempts gave unstable closed-loop.
The reason is probably high sampling frequency relative to the velocity response.
Thus, \gls{fcsmpc} is excluded from the analysis.
As shown in fig. \ref{fig:Velocity_Control}, all controllers tracked the velocity reference up to the step in the external load.
The constant torque disturbance was rejected only by the \gls{pi} controller because it is the only controller with integral action.
Although the \gls{mpc} cost function penalizes $\Delta\bm{u}$, it does not have integral action and thus the steady-state error when the external torque was applied.
There are variants of \gls{mpc} that reject constant disturbance, but they are not considered here.
Note that the reference on the quadrature current is available only for the cascaded structures.
The single-loop \gls{ccsmpc} showed the fastest response.
% Figure
\begin{figure}[tb]
	%\begin{center}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/pid_omega.pdf}
  			\subcaption{\gls{pi} controller, $\omega$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpc_omega.pdf}
  			\subcaption{\gls{ccsmpc}, $\omega$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpccas_omega.pdf}
  			\subcaption{Cascaded \gls{ccsmpc}, $\omega$.}
		\end{subfigure}
		\par\bigskip
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/pid_iq.pdf}
  			\subcaption{\gls{pi} controller, $i_{\textrm{q}}$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpc_iq.pdf}
  			\subcaption{\gls{ccsmpc}, $i_{\textrm{q}}$.}
		\end{subfigure}
		\begin{subfigure}[b]{0.32\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpccas_iq.pdf}
  			\subcaption{Cascaded \gls{ccsmpc}, $i_{\textrm{q}}$.}
		\end{subfigure}
	  	\caption{Velocities and quadrature currents durint velocity tracking. \label{fig:Velocity_Control}}
	%\end{center}
\end{figure}





\section{Position Control}
Position control using single-loop fast \gls{ccsmpc} turned out to be challenging and is therefore excluded.
The \gls{pi} and cascaded \gls{fcsmpc} responses are in fig. \ref{fig:pos_track}.
The overshoot of the \gls{fcsmpc} could be reduced if properly tuned.

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.3\textwidth]{figs/position_tracking/pid_theta.pdf}
		\caption{Position tracking.\label{fig:pos_track}}
	\end{center}
\end{figure}

