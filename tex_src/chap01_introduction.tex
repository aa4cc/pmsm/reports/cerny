\chapter{Introduction}
% This chapter states motivation, goal, thesis structure, electric motor classification, and state of the art.


% Motivation why to control PMSM/BLDC
\Glspl{pmsm} and \gls{bldc} motors are three-phase motors with permanent magnets on the rotor and the phase windings on the stator, which makes the motor construction commutator-free.
The absence of commutator ensures energy efficiency and mechanical simplicity, and thus makes these motors more reliable than their brushed counterparts as there are fewer components that may wear out.
\Glspl{pmsm} and \gls{bldc} motors also have a relatively high torque-to-volume ratio \cite{KrauseBook}.
Another advantage is that they allow for reducing the magnitude of the magnetic field inside the motor by means of a technique known as field weakening, which in turn enables operation at higher speeds.
For these reasons, \glspl{pmsm} and \gls{bldc} motors are a suitable option for torque, velocity, or position control in many industrial applications.


% Problems to deal with when controlling PMSMs
The advantages of \glspl{pmsm} and \gls{bldc} motors come at the cost of the increased complexity of control methods.
The commutation, i.e., the process of varying currents through individual phases, is realized electrically without an electromechanical commutator.
Usually, a three-phase inverter is employed together with a digital controller that operates the switching elements in the inverter.
The digital controller must operate at a sufficiently high frequency determined primarily by the motor parameters and the requirements on the precision of control.


% Classical control methods using modulation 
Many \gls{pmsm}/\gls{bldc} motor control algorithms neglect the switching nature of the inverter and generate three phase voltages in the form of three real numbers.
The generated numbers are then translated into two-state signals driving the switching elements in the inverter.
The translation utilizes a modulation scheme such as \gls{pwm} or \gls{svm}.
These algorithms include scalar control and field-oriented \gls{pid} controller.


% Direct methods and FCS-MPC
There is another class of control algorithms that take the switching nature into account and generate two-state signals directly.
\Gls{dtc} is an example.
Another example is a combination of \gls{dtc} and \gls{mpc} called \gls{fcsmpc}.
It formulates the control task as an optimization problem with binary decision variables, a problem belonging to the class of integer programs.
In this context, \gls{mpc} with real-valued decision variables is sometimes referred to as \gls{ccsmpc} \cite{CiminiMPCOnlinePMSM, AhmedFCSMPCxCCSMPC}.


% CCS-MPC and FCS-MPC advantages and disadvantages
\Gls{fcsmpc} and \gls{ccsmpc} provide a framework to deal with the control of, possibly nonlinear, constrained \gls{mimo} systems.
They both find optimal control over a finite time horizon with the possibility to take into consideration time-varying reference.
All these advantages can be exploited to control \glspl{pmsm} and \gls{bldc} motors.
Also, a subtle selection of the cost function in an \gls{mpc} algorithm can result in an inherent flux weakening at higher speeds.
When compared to continuous-control-set algorithms, \gls{fcsmpc} may yield an improved performance provided that it operates at a significantly higher frequency.
Higher sampling frequency does not necessarily mean higher switching frequency, but it provides the controller more options to select optimal time instants for the switching events.
In fact, \gls{fcsmpc} provides a convenient way to select a trade-off between the performance and the maximum switching frequency.
Low switching frequencies usually yield significant torque ripple whereas high switching frequencies yield smoother torque.
On the other hand, high switching frequencies are also associated with significant power losses.
\Gls{fcsmpc} does not keep fixed switching frequency, unlike in the case of algorithms using modulators.
In some applications, this is considered a problem  because of the possibility of low-order harmonics being propagated into the grid \cite{VazquezMPCinPowerElectronicsReview}.
\Gls{fcsmpc} is often implemented using very short control horizons of length 1 or 2.
The reasons are high sampling frequency and the computational burden of finding the optimal solution, which grows exponentially with the length of the prediction horizon.


% State goal
This thesis describes the following control algorithms for \glspl{pmsm} presented in the available literature: field-oriented \gls{pi} controller, \gls{ccsmpc}, and \gls{fcsmpc}.
The algorithms were tested in simulation and both single-loop and multi-loop cascaded structures were analyzed.
The tests examined the ability to track torque, velocity, and position references under no-load conditions and with an external load torque.
Suitable platform for hardware implementation was chosen, however, verification of the algorithms on an actual \gls{pmsm} have not been done yet.
All simulation files are provided in the attachment to this thesis.
%and are listed in appendix \ref{app:Repository}.



% Thesis structure
%The thesis is organized as follows.
%Section~\ref{sec:MotorClassification} puts \glspl{pmsm} and \gls{bldc} motors into the context of other types of electric motors and briefly describes some of them.
%Related work is discussed in section~\ref{sec:RelatedWork}.
%The mathematical model of \gls{pmsm} is given in chapter~\ref{chap:MotorModel}.
%Tested control algorithms are described in chapter~\ref{chap:Control}.
%The issues regarding hardware implementation, state estimation, and computational delay compensation are covered in chapter~\ref{chap:Implementation}.
%Chapter~\ref{chap:Simulation} evaluates the simulation results, and chapter~\ref{chap:Experiments} evaluates the results of experiments on an actual \gls{pmsm}.
%A summary and final remarks are given in chapter~\ref{chap:Conclusion}.
%Appendix \ref{app:Transformations} gives some further notes on Clarke and Park transformations.
%Appendix \ref{app:Identification} describes the identification procedure used to obtain motor parameters. 
%Appendix \ref{app:BootstrapDesign} addresses the issues associated with the control of inverters with limited on-time of the high-side switches.



\section{Classification of Electric Motors \label{sec:MotorClassification}}


% Possible classification of electric motors
A possible classification of the basic types of electric motors based on the principle of their operation is given in \cite{HanselmanBook} and is shown in fig. \ref{fig:ClassOfElMots}.

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=1\textwidth]{figs/motor_classification/mot_class.pdf}
		\caption{Classification of electric motors. Adapted with permission from \cite[Figure~1.1]{HanselmanBook}.\label{fig:ClassOfElMots}
		% \\* \textcopyright\ 2006 by Duane Hanselman. Adapted with permission. 
		}
	\end{center}
\end{figure}


% DC motors
Perhaps the simplest and the easiest to control are permanent magnet \gls{dc} motors.
Specifically, the application of constant voltage results in constant steady-state velocity.
When the stator permanent magnets are replaced by controlled winding, one gains an extra degree of freedom, which allows for field weakening.
Such motors are called separately excited \gls{dc} motors.
The main drawback of \gls{dc} motors is the presence of commutator, which makes them less efficient than \glspl{pmsm} and \gls{bldc} motors.


% Induction motors
Induction motors are more similar to \glspl{pmsm} and \gls{bldc} motors.
The difference is that induction motors do not have permanent magnets on the rotor.
Instead, the rotor is made of soft magnetic material that enables the induction of rotor currents by commutating the stator currents.
Such a design requires asynchronous operation.
In other words, the rotor is required to rotate at a different velocity than the magnetic field generated by the stator and the difference determines the generated torque.
For high-performance control, the induced rotor currents must be estimated.
Thus, control of induction motors is slightly more complicated than control of 
\glspl{pmsm} and \gls{bldc} motors.

% Stepper motors
Permanent-magnet stepper motors are very similar to \glspl{pmsm} and \gls{bldc} motors.
As shown in \cite{ChiassonBook}, their mathematical model is identical to the two-phase equivalent model of \gls{pmsm}, and therefore the same control algorithms may be applied.
A high number of pole pairs in the stepper motors makes open-loop position control reasonably precise.
Nonetheless, closed-loop feedback control greatly improves performance and efficiency.


% PMSMs and BLDC motors
The difference between \glspl{pmsm} and \gls{bldc} motors is in the back \gls{emf}, i.e., the voltage%
\footnote{
Here, no distinction between voltage and \gls{emf} is made based on the remark in  \cite[Section~1.5.1]{ChiassonBook}.
}
induced in the phase windings by the rotating permanent magnets.
\Glspl{pmsm} have a sinusoidal shape of the back \gls{emf} at the constant speed, whereas in the case of \gls{bldc} motors, the shape is trapezoidal \cite{ChiassonBook}.
The trapezoidal shape makes the motors suitable for six-step operation \cite{GiriBook}, which is easy and cheap to implement.
The reason \gls{bldc} motors are called brushless \gls{dc} is that they often come with a current controller, and the overall closed-loop system can be controlled in a similar way as a \gls{dc} motor \cite{ChiassonBook}.
It is worth noting that some texts do not make the distinction between \glspl{pmsm} and \gls{bldc} motors, e.g. \cite{KrauseBook}.
Also, some \glspl{pmsm} and \gls{bldc} motors have neither sinusoidal nor trapezoidal shape of the back \gls{emf} and the shape is described by more general periodic function.
Control of such motors is addressed in \cite{MoehleWaveformsForBLDC}.

% BLDCs have 15% more power density (Krishnan 48 ... 1.4.5: For equal resistive losses, it has 15.4% higher power density than the PMSMs.) but harder to make truly trapezoidal winding -> smaller ripple current and ripple torque in case of PMSM, BLDC - simpler control, it is sufficient to distinguis six position regions, Hall sensor detecting these position and discrete positions at transitions


\section{Related Work \label{sec:RelatedWork}}


% Overviews of MPC
Now, a short list of relevant work done by others is given.
A comprehensive overview of \gls{ccsmpc} and \gls{fcsmpc} algorithms applied in different areas of power electronics is given in \cite{VazquezMPCinPowerElectronicsReview}.
A possible implementation of \gls{ccsmpc} for \gls{pmsm} with \gls{hil} verification was presented by authors of \cite{CiminiMPCOnlinePMSM}.
A comparison of \gls{fcsmpc} and \gls{ccsmpc} is given in \cite{AhmedFCSMPCxCCSMPC}, where both algorithms showed similar results.
An overview focused on \gls{fcsmpc} is given in \cite{RodriguezMPCFCSReview}.
A detailed description of one-step \gls{fcsmpc} implementations used for different topologies of power converters is given in \cite{RodriguezBook}.
Several variants of \gls{fcsmpc} with a long prediction horizon designed for medium-voltage induction motors are described in \cite{GeyerBook}.
Also, the author of \cite{GeyerBook} shows that \gls{fcsmpc} applied to a three-level power converter can both reduce the switching losses and improve the performance at the same time when compared to algorithms with voltage modulation stage.
In contrast, in this thesis, \gls{fcsmpc} is designed for low-voltage \gls{pmsm} with a two-level inverter.


  